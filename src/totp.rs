use std::io::{self, Read};
use std::process;

mod encoding;
use encoding::Base32;
mod hotp;
use hotp::Totp;

fn main() {
    let mut buf: Vec<u8> = Vec::new();
    let result = match io::stdin().read_to_end(&mut buf) {
        Ok(n) if n > 0 => Totp::get(&Base32::decode(&buf)),
        _ => process::exit(1),
    };

    println!("{:?}", result);
}

