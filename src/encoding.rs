
pub struct Base32;

impl Base32 {
    pub fn decode(input: &[u8]) -> Vec<u8> {
        let mut output = Vec::with_capacity(input.len() * 5 / 8);
        let cleaned_input = input.iter().filter_map(|&b| {
            match b {
                65...90 => Some(b - 65), // A...Z
                97...122 => Some(b - 97), // a...z
                50...55 => Some(b - 24), // 2...7
                _ => None,
            }
        });

        let mut acc = 0;
        for (i, b) in cleaned_input.enumerate() {
            match i % 8 {
                0 => acc = (b & 0b11111) << 3,
                1 => {
                    output.push(acc | ((b & 0b11100) >> 2));
                    acc = (b & 0b00011) << 6;
                },
                2 => acc |= (b & 0b111111) << 1,
                3 => {
                    output.push(acc | ((b & 0b10000) >> 4));
                    acc = (b & 0b01111) << 4;
                },
                4 => {
                    output.push(acc | ((b & 0b11110) >> 1));
                    acc = (b & 0b00001) << 7;
                },
                5 => acc |= (b & 0b11111) << 2,
                6 => {
                    output.push(acc | ((b & 0b11000) >> 3));
                    acc = (b & 0b00111) << 5;
                },
                7 => output.push(acc | (b & 0b11111)),
                _ => continue,
            }
        }
        output
    }
}

