extern crate crypto;
use self::crypto::hmac::Hmac;
use self::crypto::mac::Mac;
use self::crypto::sha1::Sha1;

use std::time::UNIX_EPOCH;

pub struct Totp;

impl Totp {
    pub fn get(key: &[u8]) -> u32 {
        let mut counter: u64 = UNIX_EPOCH
            .elapsed()
            .unwrap()
            .as_secs() / 30;
        let counter_bytes: &mut [u8; 8] = &mut [0u8; 8];
        for i in (0..8).rev() {
            counter_bytes[i] = (counter & 0xff) as u8;
            counter >>= 8;
        }

        let hmac = {
            let mut hmac: Hmac<Sha1> = Hmac::new(Sha1::new(), key);
            hmac.input(counter_bytes);
            hmac.result()
        };
        let hash: &[u8] = hmac.code();
        let offset: usize = (hash.last().unwrap() & 0xf) as usize;
        let (a, b, c, d) = (
            hash[offset] as u32,
            hash[offset + 1] as u32,
            hash[offset + 2] as u32,
            hash[offset + 3] as u32
        );
        let otp: u32 = ((a & 0x7f) << 24) | ((b & 0xff) << 16) | ((c & 0xff) << 8) | (d & 0xff);
        otp % 1_000_000
    }
}

